const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let login = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(login).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 700}); 
    await page.screenshot({path: 'test/screenshots/welcom.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 700}); 
    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
     //TODO

})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');
    await browser.close();
}
)

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Room1', 100);
    
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('Room1');
    await browser.close();

})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    expect(member).toBe('John');
    
    const page2 = await browser.newPage();
    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#users > ol > li');    
    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member2).toBe('Mike');
       
    
    await browser.close();

})

// 11
test('[Content] The "Send" button should exist', async () => {
    //TODO
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
 
    let send = await page.$eval('body > div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    expect(send).toBe('Send');
    await browser.close(); 
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 300});
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hi', {delay: 10});
    await page.keyboard.press('Enter', {delay: 100});
    let msg = await page.$eval('body > div:nth-child(2) > ol > li:nth-child(2) > div:nth-child(2) > p', (content) => content.innerHTML);
    expect(msg).toBe('Hi');
    await browser.close();

})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    //TODO
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    const page2 = await browser.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hi', {delay: 10});
    await page.keyboard.press('Enter', {delay: 10});
    await page2.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hello', {delay: 10});
    await page2.keyboard.press('Enter', {delay: 10});
//    let name = await page.$eval('body > div:nth-child(2) > ol > li:nth-child(4) > div:nth-child(1) > h4', (content) => content.innerHTML);
    let msg = await page.$eval('body > div:nth-child(2) > ol > li:nth-child(4) > div:nth-child(2) > p', (content) => content.innerHTML);
    expect(msg).toBe('Hello');
//    expect(name).toBe('Mike');
    

//    let name2 = await page2.$eval('body > div:nth-child(2) > ol > li:nth-child(2) > div:nth-child(1) > h4', (content) => content.innerHTML);
    let msg2 = await page2.$eval('body > div:nth-child(2) > ol > li:nth-child(2) > div:nth-child(2) > p', (content) => content.innerHTML);
//    expect(name2).toBe('John');
    expect(msg2).toBe('Hi');
    await browser.close();

})

// 14
test('[Content] The "Send location" button should exist', async () => {
    //TODO
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    let send = await page.$eval('body > div:nth-child(2) > div > button', (content) => content.innerHTML);
    expect(send).toBe('Send location');
    await browser.close();


})

// 15
test('[Behavior] Send a location message', async () => {
     //TODO
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {
delay: 100});
    await page.keyboard.press('Enter', {delay: 300});
    await page.click('body > div:nth-child(2) > div > button', {delay:100});
    //let send = await page.$eval('body > div:nth-child(2) > div > button', (content) => content.innerHTML);
  //  expect(send).toBe('Send location');
    //await browser.close();

})
